import gitlab
import subprocess
import tqdm
import os
import settings

logins = """Kirill456Z
mendac1um
ilya.kochagin
nikita1877
miasnikov.km
petrovma
Nastya_H
Korifeyi
sofism
zieonasta
tkharisov7
denisonzierpka
Shtubbb""".split()

surnames = """Zemlyanskiy
Kolesnikov
Kochagin
Moiseev
Myasnikov
Petrov
Polovinkina
Rodionov
Samoylova
Frolov
Harisov
Cirpka
Shtuba""".split()

group_name = "mipt-2020-algorithms-023-025"


def get_repo_name(surname):
    repo_name_format = "algo-2020-1sem-{}"
    return repo_name_format.format(surname)


def is_repo_initialised(user):
    return False


def main():
    gl = gitlab.Gitlab(url="https://gitlab.com", oauth_token=settings.token)
    group_id = gl.groups.list(search=group_name)[0].id

    for surname, login in tqdm.tqdm(zip(surnames, logins)):
        repo_name = get_repo_name(surname)
        user = gl.users.list(username=login)[0]

        if is_repo_initialised(user):
            continue

        project = gl.projects.create({
            'name': repo_name,
            'namespace_id': group_id
        })
        project.members.create({
            'user_id': user.id,
            'access_level': gitlab.DEVELOPER_ACCESS
        })

        subprocess.run(["git", "clone", f"git@gitlab.com:{group_name}/{repo_name}.git"])
        subprocess.run(["cp", ".gitlab-ci.yml", f"{repo_name}/"])
        os.chdir(repo_name)
        subprocess.run(
            ["git", "submodule", "add", "https://gitlab.com/rationalex/mipt-algorithms-course-infrastructure.git"]
        )
        subprocess.run(["git", "add", "."])
        subprocess.run(["git", "commit", "-m", "initial"])
        subprocess.run(["git", "push"])

        # CLEANUP

        os.chdir("..")
        subprocess.call(["rm", "-rf", repo_name])


if __name__ == "__main__":
    main()
